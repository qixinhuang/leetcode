package LC142;

/**
 * Created by lipingzhang on 4/4/17.
 */

class ListNode {
    int val;
    ListNode next;
    ListNode(int x) { val = x; }
}

public class Solution1 {
    public static ListNode detectCycle(ListNode head) {
        if (head == null) {
            return null;
        }

        // check cycle
        ListNode move = head;
        // Java passes address of referrence (passing copy of referrence),  not referrence itself
        // so needs another array
        ListNode[] extra = new ListNode[1];
        if (checkCycle(move, extra)) {
            // detect cycle beginning
            move = extra[0];
            while (move != head) {
                head = head.next;
                move = move.next;
            }

            return move;
        }

        return null;
    }

    private static boolean checkCycle(ListNode move, ListNode[] extra) {
        if (move == null) {
            return false;
        }
        ListNode fast = move;
        while (fast != null && fast.next != null) {
            move = move.next;
            fast = fast.next.next;
            if (move == fast) {
                extra[0] = move;
                return true;
            }
        }

        return false;
    }

    public static void main(String[] args) {
        ListNode node1 = new ListNode(3);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(0);
        ListNode node4 = new ListNode(-4);
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node2;

        ListNode ans = detectCycle(node1);

        System.out.println(ans.val);
    }
}
