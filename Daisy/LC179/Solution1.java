package LC179;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by lipingzhang on 4/6/17.
 */
public class Solution1 {
    public String largestNumber(int[] nums) {
        if (nums == null || nums.length == 0) {
            return null;
        }

        String[] strs = new String[nums.length];
        for (int i = 0; i < nums.length; i++) {
            strs[i] = String.valueOf(nums[i]);
        }

        Comparator<String> newComp = new Comparator<String>(){
            public int compare(String s1, String s2) {
                String sum1 = s1 + s2;
                String sum2 = s2 + s1;
                // use compareTo, sum2.compareTo(sum1)
                return sum2.compareTo(sum1);
            }
        };

        Arrays.sort(strs, newComp);

        // use equal, not ==
        if (strs[0].equals("0")) {
            return "0";
        }

        StringBuilder sb = new StringBuilder();
        for (String s : strs) {
            sb.append(s);
        }

        return sb.toString();
    }
}
