package LC203;

/**
 * Created by lipingzhang on 4/7/17.
 */
public class Solution2 {
    public ListNode removeElements(ListNode head, int val) {
        if (head == null) {
            return head;
        }
        head.next = removeElements(head.next, val);
        return head.val == val ? head.next : head;
    }
}
