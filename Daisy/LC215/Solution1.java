package LC215;

/**
 * Created by lipingzhang on 4/6/17.
 */
public class Solution1 {
    public int findKthLargest(int[] nums, int k) {
        if (nums == null || nums.length == 0 || k > nums.length || k <= 0)  {
            return 0;
        }
        int n = nums.length;
        return helper(nums, 0, n - 1, n - k + 1);
    }

    private int helper(int[] nums, int start, int end, int k) {
        int m = partition(nums, start, end);
        // m + 1, not m - 1
        if (m + 1 < k) {
            // m + 1
            return helper(nums, m + 1, end, k);
        } else if (m + 1 > k) {
            // m - 1
            return helper(nums, start, m - 1, k);
        } else {
            return nums[m];
        }
    }

    private int partition(int[] nums, int start, int end) {
        int pos = nums[start];

        while (start < end) {
            // >=, not >
            while (start < end && nums[end] >= pos) {
                end--;
            }

            nums[start] = nums[end];
            // <=, not <
            while (start < end && nums[start] <= pos) {
                start++;
            }

            nums[end] = nums[start];
        }

        nums[start] = pos;
        return start;
    }
}
