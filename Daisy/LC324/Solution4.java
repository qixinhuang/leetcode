package LC324;

/**
 * Created by lipingzhang on 4/6/17.
 */
public class Solution4 {

    // time complexity : O(N), space complexity : O(1)
    public void wiggleSort(int[] nums) {
        if(nums == null || nums.length <= 1){
            return;
        }
        int len = nums.length;
        int median = selectKth(nums, 0, len - 1, (nums.length + 1) / 2);

        // need to think of index mapping
        int left = 0;
        int right = len - 1;
        int i = 0;
        while (i <= right) {
            if (nums[newIndex(i, len)] > median) {
                swap(nums, newIndex(left++, len), newIndex(i++, len));
            } else if (nums[newIndex(i, len)] < median) {
                swap(nums, newIndex(right--, len), newIndex(i, len));
            } else {
                i++;
            }
        }
    }

    private int newIndex(int index, int n) {
        return (1 + 2 * index) % (n | 1);
    }

    private void swap(int[] nums, int a, int b) {
        int tmp = nums[a];
        nums[a] = nums[b];
        nums[b] = tmp;
    }

    // need to think of this implementation
    private int selectKth(int[] nums, int low, int high, int k) {
        int start = low;
        int end = high;
        int pivot = nums[k];
        while (start <= end) {
            while (nums[start] < pivot) {
                start++;
            }
            while (nums[end] > pivot) {
                end--;
            }
            if (start == k && end == k) {
                return nums[k];
            }

            if (start <= end) {
                swap(nums, start, end);
                start++;
                end--;
            }
        }

        if (start <= k) {
            return selectKth(nums, start, high, k);
        }
        return selectKth(nums, low, end, k);
    }


    // this is the normal implementation
    private int selectKth2(int[] nums, int start, int end, int k) {
        int m = partition(nums, start, end);
        if (m + 1 < k) {
            return selectKth(nums, m + 1, end, k);
        } else if (m + 1 > k) {
            return selectKth(nums, start, m - 1, k);
        } else {
            // return value
            return nums[m];
        }
    }

    private int partition(int[] nums, int start, int end) {
        int pos = nums[start];
        while (start < end) {
            while (start < end && nums[end] >= pos) {
                end--;
            }
            nums[start] = nums[end];
            while (start < end && nums[start] <= pos) {
                start++;
            }
            nums[end] = nums[start];
        }
        nums[start] = pos;
        return start;
    }
}
