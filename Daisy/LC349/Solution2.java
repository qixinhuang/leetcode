import java.util.HashSet;

/**
 * Created by lipingzhang on 3/31/17.
 */
public class Solution2 {
    public int[] intersection(int[] nums1, int[] nums2) {
        HashSet<Integer> set = new HashSet<>();
        HashSet<Integer> intersect = new HashSet<>();
        for(int n1 : nums1){
            set.add(n1);
        }

        for(int n2: nums2){
            if(set.contains(n2)){
                intersect.add(n2);
            }
        }

        int[] ans = new int[intersect.size()];
        int i = 0;
        for(int s : intersect){
            ans[i++] = s;
        }
        return ans;
    }
}
