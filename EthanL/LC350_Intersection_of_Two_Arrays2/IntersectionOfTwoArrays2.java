import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by demon on 2017/4/1.
 */
public class IntersectionOfTwoArrays2 {
    public int[] intersect(int[] num1, int[] num2) {
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < num1.length; i++) {
            if (map.containsKey(num1[i])) {
                map.put(num1[i], map.get(num1[i]) + 1);
            } else {
                map.put(num1[i] , 1);
            }
        }
        List<Integer> resultList = new ArrayList<Integer>();
        for (int i = 0; i < num2.length; i++) {
            if (map.containsKey(num2[i])) {
                if (map.get(num2[i]) > 0) {
                    resultList.add(num2[i]);
                    map.put(num2[i], map.get(num2[i]) - 1);
                }
            }
        }
        int result[] = new int[resultList.size()];
        int index = 0;
        for (int i : resultList) {
            result[index] = i;
            index++;
        }
        return result;
    }
}
