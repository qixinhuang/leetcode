import javax.xml.soap.Node;

/**
 * Created by ethan on 4/12/17.
 */
public class Stack<T> {
    private int capacity;
    public Node top, bottom;
    public int size = 0;

    public Stack(int capacity) {
        this.capacity = capacity;
    }

    public boolean isFull() {return capacity == size;}

    public boolean isEmpty() {return size == 0;}

    public void join(Node above, Node below) {
        if (below != null) below.above(above);
        if (above != null) above.below(below);
    }

    public boolean push(int x) {
        if (size >= capacity) return false;
        size++;
        Node n = new Node(x);
        if (size == 1) bottom = n;
        join(n, top);
        top = n;
        return true;
    }

    public int pop() {
        Node t = top;
        top = top.below;
        size--;
        return t.value;
    }

    public int removeBottom() {
        Node b = bottom;
        bottom = bottom.above;
        if (bottom != null) bottom.below = null;
        size--;
        return b.value;
    }
    class Node {
        public Node below;
        public Node above;
        public int value;
        public Node(int v) {
            this.value = v;
        }

        public void below(Node below) {
            this.below = below;
        }

        public void above(Node above) {
            this.above = above;
        }
    }
}
