import java.util.HashMap;

/*
 * LeetCode 138 Copy List with random pointer
 * A linked list is given such that each node contains an additional random
 * pointer which could point to any node in the list or null.
 * Return a deep copy of the list
 */
public class CopyListWithRandomPointer {
    /*
    *  Solution #1:
    *  1. copy every node, duplicate every node and insert it to the list
    *  2. copy random pointers for all newly created nodes
    *  3. break the list into two
    * */
    public RandomListNode copyRandomList (RandomListNode head) {
        if (head == null) return head;

        RandomListNode p = head;

        // copy every node and insert to list
        while (p != null) {
            RandomListNode copy = new RandomListNode(p.label);
            copy.next = p.next;
            p.next = copy;
            p = copy.next;
        }

        // copy random pointer for each node
        while (p != null) {
            if (p.random != null) {
                p.next.random = p.random.next;
            }
            p = p.next.next;
        }

        p = head;
        RandomListNode newHead = head.next;
        while (p != null) {
            RandomListNode temp = p.next;
            p.next = temp.next;
            if (temp.next != null) {
                temp.next = temp.next.next;
            }
            p = p.next;
        }
        return newHead;
    }

    /*
    * Solution #2:
    * We can use a HashMap which makes it simpler
    * */
    public RandomListNode copyRandomListHash (RandomListNode head) {
        if (head == null) return head;
        HashMap<RandomListNode, RandomListNode> map = new HashMap<RandomListNode, RandomListNode>();
        RandomListNode newHead = new RandomListNode(head.label);

        RandomListNode p = head;
        RandomListNode q = newHead;
        map.put(head, newHead);

        p = p.next;
        while (p != null) {
            RandomListNode temp = new RandomListNode(p.label);
            map.put(p, temp);
            q.next = temp;
            q = temp;
            p = p.next;
        }

        p = head;
        q = newHead;
        while (p != null) {
            if (p.random != null)
                q.random = map.get(p.random);
            else
                q.random = null;
            p = p.next;
            q = q.next;
        }
        return newHead;
    }
}
