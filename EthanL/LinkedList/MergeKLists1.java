import java.util.List;

/**
 * Created by ethan on 4/12/17.
 */

/*LC 23 Merge K sorted Lists.
Merge k sorted linked lists and return it as one sorted list.
Analyze and describe its complexity.

Solution: Divide and Conquer
*/
public class MergeKLists1 {
    public ListNode mergeKLists(ListNode[] list) {
        if (list.length == 0) {
            return null;
        }
        return mergeHelper(list, 0, list.length - 1);
    }

    private ListNode mergeHelper(ListNode[] list, int start, int end) {
        if (start == end) return list[start];

        int mid = start + (end - start) / 2;
        ListNode left = mergeHelper(list, start, mid);
        ListNode right =  mergeHelper(list, mid + 1, end);
        return mergeTwoLists(left, right);
    }

    private ListNode mergeTwoLists(ListNode left, ListNode right) {
        ListNode dummy  = new ListNode(0);
        ListNode tail = dummy;
        while (left != null && right != null) {
            if (left.val < right.val) {
                tail.next = left;
                tail = left;
                left = left.next;
            } else {
                tail.next = right;
                tail = right;
                right = right.next;
            }
        }
        if (left != null) {
            tail.next = left;
        } else {
            tail.next = right;
        }
        return dummy.next;
    }
}
