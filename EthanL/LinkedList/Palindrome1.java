/*
 * CC189 2.6 Palindrome: Implement a function to check if a linked list is a palindrome
 */
public class Palindrome1 {
    // Solution # 1, Reverse and Compare
    public boolean isPalindrome(ListNode head) {
        ListNode reversed = reverseAndClone(head);
        return isEqual(head, reversed);
    }

    private boolean isEqual(ListNode head1, ListNode head2) {
        while (head1 != null && head2 != null) {
            if (head1.val != head2.val) return false;
            head1 = head1.next;
            head2 = head2.next;
        }
        return head1 == null && head2 == null;
    }

    private ListNode reverseAndClone(ListNode node) {
        ListNode head = null;
        // Connect cloned linked list from back to head
        while (node != null) {
            ListNode n = new ListNode(node.val);
            n.next = head;
            head = n;
            node = node.next;
        }
        return head;
    }
}
