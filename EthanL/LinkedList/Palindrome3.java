/*
 * CC189 2.6 Palindrome Third Solution Recursive
 *
 */
public class Palindrome3 {
    class Result {
        public ListNode node;
        public boolean result;

        public Result(ListNode next, boolean b) {
            node = next;
            result = b;
        }
    }

    boolean isPalindrome(ListNode head) {
        int length = lengthOfList(head);
        Result p = isPalindromeRecurse(head, length);
        return p.result;
    }

    private Result isPalindromeRecurse(ListNode head, int length) {
        if (head == null || length <= 0) {
            return new Result(head, true);
        } else if (length == 1) {
            return new Result(head.next, true);
        }


        Result res = isPalindromeRecurse(head.next, length - 2);

        if (!res.result || res.node == null) {
            return res;
        }

         /* Check if matches corresponding node on other side. */
        res.result = (head.val == res.node.val);

        res.node = res.node.next;

        return res;
    }

    private int lengthOfList(ListNode n) {
        int size = 0;
        while (n != null) {
            size++;
            n = n.next;
        }
        return size;
    }
}

