import java.util.HashSet;

/*
 * cc189 2.1 Remove Duplicates from linked list
 */
public class RemoveDuplicates {
    void deleteDups(ListNode n) {
        HashSet<Integer> set = new HashSet<>();
        ListNode previous = null;
        while (n != null) {
            if (set.contains(n.val)) {
                previous.next = n.next;
            } else {
                set.add(n.val);
                previous = n;
            }
            n = n.next;
        }
    }
    // What is no buffer is allowed?
    void deleteDupsNoBuffer(ListNode head) {
        ListNode current = head;
        while(current != null) {
            ListNode runner = current;
            while(runner.next != null) {
                if (runner.val == current.val) {
                    runner.next = runner.next.next;
                } else {
                    runner = runner.next;
                }
            }
        }
    }
}
