import java.util.ArrayList;
import java.util.List;

/**
 * Created by ethan on 4/18/17.
 */
public class BinaryTreeLevelOrderTraversal2 {
    // Solution # 2: DFS
    public List<List<Integer>> levelOrder(TreeNode root) {
        ArrayList results = new ArrayList();
        if (root == null) return results;
        int maxLevel = 0;
        while (true) {
            ArrayList<Integer> level = new ArrayList<>();
            dfs(root, level, 0, maxLevel);
            if (level.size() == 0) break;
            results.add(level);
            maxLevel++;
        }
        return results;
    }

    private void dfs(TreeNode root, ArrayList<Integer> level, int curLevel, int maxLevel) {
        if (root == null || curLevel > maxLevel) {
            return;
        }
        if (curLevel == maxLevel) {
            level.add(root.val);
            return;
        }
        dfs(root.left, level, curLevel + 1, maxLevel);
        dfs(root.right, level, curLevel + 1, maxLevel);
    }
}