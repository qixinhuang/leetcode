/**
 * Created by ethan on 4/11/17.
 */
public class InvertBinaryTree {
    public TreeNode invertTree(TreeNode root) {
        invertBinaryTree(root);
        return root;

    }
    public void invertBinaryTree(TreeNode root) {
        if (root == null) return;
        TreeNode temp = root.left;
        root.left = root.right;
        root.right = temp;

        invertBinaryTree(root.left);
        invertBinaryTree(root.right);

    }
}
