import java.util.LinkedList;

/**
 * Created by ethan on 4/17/17.
 */

/*
* LC 101 Symmetric Tree
* Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).
* Bonus points if you could solve it both recursively and iteratively.

* */
public class SymmetricTree {
    //Solution 1: Iterative, check every level to see if symmetric
    public boolean isSymmetric(TreeNode root) {
        if (root == null) return true;
        if (root.left == null && root.right == null) return true;
        if (root.left == null || root.right == null) return false;
        LinkedList<TreeNode> q1 = new LinkedList<TreeNode>();
        LinkedList<TreeNode> q2 = new LinkedList<TreeNode>();
        q1.add(root.left);
        q2.add(root.right);
        while(!q1.isEmpty() && !q2.isEmpty()) {
            TreeNode n1 = q1.poll();
            TreeNode n2 = q2.poll();

            if (n1.val != n2.val) return false;
            if ((n1.left == null && n2.right != null) || (n1.left != null
            && n2.right == null)) return false;
            if ((n1.right == null && n2.left != null) || (n1.right != null
            && n2.left == null)) return false;

            if (n1.left != null && n2.right != null) {
                q1.add(n1.left);
                q2.add(n2.right);
            }

            if (n1.right != null && n2.left != null) {
                q1.add(n1.right);
                q2.add(n2.left);
            }
        }
        return true;
    }

    public boolean isSymmetric2(TreeNode root) {
        if (root == null) return true;
        return isSymmetricHelper(root.left, root.right);
    }

    private boolean isSymmetricHelper(TreeNode left, TreeNode right) {
        if (left == null && right == null) return true;
        if (left == null || right == null) return false;
        return (left.val == right.val)
                && isSymmetricHelper(left.left, right.right)
                && isSymmetricHelper(left.right, right.left);
    }
}
