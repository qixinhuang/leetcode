'''
234. Palindrome Linked List

Given a singly linked list, determine if it is a palindrome.

Follow up:
Could you do it in O(n) time and O(1) space?

'''

# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def isPalindrome(self, head):
        """
        :type head: ListNode
        :rtype: bool
        """
        if head is None:
            return True
        
        slow = fast = head
        while fast.next and fast.next.next: ###Find midpoint
            slow= slow.next
            fast= fast.next.next
        
        sec_half = slow.next
        reversed_sec_half = ListNode(0)
        
        #reverse second_half
        while sec_half:
            dummy = sec_half.next
            sec_half.next = reversed_sec_half
            reversed_sec_half = sec_half
            sec_half = dummy
            
        # check if palindrome
        d1 = reversed_sec_half
        d2 = head
        while d1.next and (d1.val == d2.val): ### check if the palindrome, true is d1(i.e. the reversed_sec_half is the same with the first half( the last element will be none), otherwise the check breaks and the none element will not be reached. 
            d1 = d1.next
            d2 = d2.next
        return d1.next is None
            
        
        
        