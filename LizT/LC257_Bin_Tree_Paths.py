'''
257. Binary Tree Paths
Given a binary tree, return all root-to-leaf paths.

For example, given the following binary tree:

   1
 /   \
2     3
 \
  5
All root-to-leaf paths are:

["1->2->5", "1->3"]


'''

class Solution(object):
    def binaryTreePaths(self, root):
        """
        :type root: TreeNode
        :rtype: List[str]
        """
        if not root:
            return []
        self.out = []
        self.append_path(root, str(root.val))
        return self.out
        
    def append_path(self, root, path):
        if (root.left == None) and (root.right==None):
            self.out += path,   ### comma is a must
            return self.out
        if root.left: 
            self.append_path(root.left, path+ '->' +str(root.left.val))
        if root.right:
            self.append_path(root.right, path + '->' + str(root.right.val))