'''
501. Find Mode in Binary Search Tree

Given a binary search tree (BST) with duplicates, find all the mode(s) (the most frequently occurred element) in the given BST.

Assume a BST is defined as follows:

The left subtree of a node contains only nodes with keys less than or equal to the node's key.
The right subtree of a node contains only nodes with keys greater than or equal to the node's key.
Both the left and right subtrees must also be binary search trees.
For example:
Given BST [1,null,2,2],
   1
    \
     2
    /
   2
return [2].

Note: If a tree has more than one mode, you can return them in any order.
'''

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def findMode(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        if not root:
            return []
        self.dic = collections.Counter()
        self.traverse(root)
        maxnum = max(self.dic.values())
        mode = []
        for k, v in self.dic.iteritems():
            if v == maxnum: 
                mode.append(k)
        return mode
        
    def traverse(self, root):
        if not root:
            return None
        self.dic[root.val] += 1
        self.traverse(root.left)
        self.traverse(root.right)
    