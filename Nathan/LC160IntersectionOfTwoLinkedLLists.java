package LinkedList;

/**
 * Created by Nate He on 4/8/2017.
 */
public class LC160IntersectionOfTwoLinkedLLists {

    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        int lenA = getListLength(headA);
        int lenB = getListLength(headB);
        if (lenA >= lenB) {
            for (int i = lenA - lenB; i > 0; i--) {
                headA = headA.next;
            }
        } else {
            for (int i = lenB - lenA; i > 0; i--) {
                headB = headB.next;
            }
        }
        while (headA != null) {
            if (headA != headB) {
                headA = headA.next;
                headB = headB.next;

            } else {
                return headA;
            }
        }
        return null;


    }

    public int getListLength(ListNode head) {
        int len = 0;
        while (head != null) {
            len = len + 1;
            head = head.next;
        }
        return len;
    }
}