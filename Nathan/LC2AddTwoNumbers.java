package LinkedList;

/**
 * Created by Nate He on 4/4/2017.
 * forget upgrade carry = carry + sum;
 */
public class LC2AddTwoNumbers {
    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
            if(l1 == null){
                return l2;
            }
            if(l2 == null){
                return l1;
            }
            ListNode dummy = new ListNode(0);
            ListNode res = dummy;

            int carry = 0;
            while(l1 != null || l2 != null){
                int sum = 0;
               if( l1 != null){
                   sum = sum + l1.val;
                   l1 = l1.next;
                }
                if(l2 != null){
                   sum = sum + l2.val;
                   l2 = l2.next;
                }
                int digit = (sum + carry)%10;
                carry = (sum + carry)/ 10;
                res.next = new ListNode(digit);
                res = res.next;

                if( l1 == null && l2 == null && carry == 1){
                    res.next = new ListNode(1);

                }
            }
            return dummy.next;

        }
        public static void main(String[] agrs){
            ListNode l1 = new ListNode(1);
            ListNode l2 = new ListNode(9);
            l2.next = new ListNode( 9);
            System.out.println(addTwoNumbers(l1,l2));

        }

}
