package LinkedList;

/**
 * Created by Nate He on 4/10/2017.
 * no fakehead;   pay attention to the last node case;
 */
public class LC83RemoveDuplicatesfromSortedList {
    public ListNode deleteDuplicates(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode curr = head;
        while (curr != null && curr.next != null) {
            ListNode next = curr.next;

            while (curr.val == next.val) {
                if (next.next != null) { // last node case;
                    next = next.next;
                    curr.next = next;
                } else {
                    curr.next = null;
                    return head;
                }
            }
            curr = curr.next;
        }
        return head;
    }

}
