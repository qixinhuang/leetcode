package LinkedList;

/**
 * Created by Nate He on 3/30/2017.
 */
public class ListNode {
    int val;
    ListNode next;
    ListNode(int x){
        val = x;
    }
}
