package Tree;

/**
 * Created by Nate He on 4/10/2017.
 */
public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }
}