//101. Symmetric Tree
// Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).

// For example, this binary tree [1,2,2,3,4,4,3] is symmetric:

//     1
//    / \
//   2   2
//  / \ / \
// 3  4 4  3
// But the following [1,2,2,null,3,null,3] is not:
//     1
//    / \
//   2   2
//    \   \
//    3    3
// Note:
// Bonus points if you could solve it both recursively and iteratively.

class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
}

public class LC101{
	public boolean isSymmetric(TreeNode root) {
        if(root == null){
            return true;
        }
        return recursiveHelper(root.left, root.right);
    }
    
    private boolean recursiveHelper(TreeNode right, TreeNode left){
        if( left == null ){
            return right == null;
        }
        
        if( right == null){
            return left == null;
        }
        
        if(left.val!=right.val){
            return false;
        }
        
        return recursiveHelper(left.left, right.right) && recursiveHelper(left.right, right.left);
    }

    public boolean isSymmetric2(TreeNode root) {//wrong
        if(root == null){
            return true;
        }
        
        Queue<TreeNode> queue = new LinkedList<>();
        if(root.left == null){
            return root.right == null;
        }
        if(root.right == null){
            return root.left == null;
        }
        queue.add(root.left);
        queue.add(root.right);
        
        TreeNode left, right;
        while(!queue.isEmpty()){
            left = queue.poll();
            right = queue.poll();
            if(left.val != right.val){
                return false;
            }
            
            if(left.left == null){
                return right.right == null;
            }
            if(right.right == null){
                return left.left == null;
            }
            queue.add(left.left);
            queue.add(right.right);
            
            if(left.right == null){
                return right.left == null;
            }
            if(right.left == null){
                return left.right == null;
            }
            queue.add(left.right);
            queue.add(right.left);
        }
        return true;
    }


    public boolean isSymmetric3(TreeNode root) {
        public boolean isSymmetric(TreeNode root) {
        if(root==null)  return true;
    
	    Queue<TreeNode> queue = new LinkedList<TreeNode>();
	    TreeNode left, right;
	    if(root.left!=null){
	        if(root.right==null) return false;
	        queue.add(root.left);
	        queue.add(root.right);
	    }
	    else if(root.right!=null){
	        return false;
	    }
	        
	    while(!queue.isEmpty()){
	        left = queue.poll();
	        right = queue.poll();
	        if(right.val!=left.val) return false;
	        
	        if(left.left!=null){
	            if(right.right==null)   return false;
	            queue.add(left.left);
	            queue.add(right.right);
	        }
	        else if(right.right!=null){
	            return false;
	        }
	            
	        if(left.right!=null){
	            if(right.left==null)   return false;
	            queue.add(left.right);
	            queue.add(right.left);
	        }
	        else if(right.left!=null){
	            return false;
	        }
	    }
	    
	    return true;
    }
}