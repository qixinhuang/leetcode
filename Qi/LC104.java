// LC104. Maximum Depth of Binary Tree
// Given a binary tree, find its maximum depth.
// The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.  

public class LC104 {

    public int maxDepth1(TreeNode root) {
        if(root == null){
            return 0;
        }
        int left = maxDepth(root.left) + 1;
        int right = maxDepth(root.right) + 1;
        return Math.max(left, right);
    }

    public int maxDepth2(TreeNode root) {
        if(root == null){
            return 0;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        Queue<Integer> queueD = new LinkedList<>();
        queue.add(root);
        queueD.add(0);
        
        TreeNode curNode;
        int curDepth = 0;
        
        while(!queue.isEmpty()){
            curNode = queue.poll();
            curDepth = queueD.poll();
            curDepth++;
            if(curNode.left!=null){
                queue.add(curNode.left);
                queueD.add(curDepth);
            }
            if(curNode.right!=null){
                queue.add(curNode.right);
                queueD.add(curDepth);
            }
        }
        return curDepth;
    }

    int max = Integer.MIN_VALUE;
    public int maxDepth3(TreeNode root) {
        if(root == null){
            return 0;
        }
        Deque<TreeNode> stack = new LinkedList<>();
        Deque<Integer> stackD = new LinkedList<>();
        stack.push(root);
        stackD.push(0);
        max = Integer.MIN_VALUE;
        TreeNode curNode;
        int curDepth = 0;
        while(!stack.isEmpty()){
            curNode = stack.pop();
            curDepth = stackD.pop();
            curDepth ++;
            max = Math.max(max, curDepth);
            if(curNode.left!=null){
                stack.push(curNode.left);
                stackD.push(curDepth);
            }
            if(curNode.right!=null){
                stack.push(curNode.right);
                stackD.push(curDepth);
            }
        }
        return max;
    }
}