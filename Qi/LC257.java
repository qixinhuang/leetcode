// LC257  Binary Tree Paths

// Given a binary tree, return all root-to-leaf paths.

// For example, given the following binary tree:

//    1
//  /   \
// 2     3
//  \
//   5
// All root-to-leaf paths are:

// ["1->2->5", "1->3"]

public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode(int x) { val = x; }
}

public class LC257{
	public List<String> binaryTreePaths(TreeNode root) {//recursive
        List<String> list = new LinkedList<>();
        if(root == null){
            return list;
        }

        if(root.left == null && root.right == null){
            list.add(Integer.toString(root.val));
            return list;
        }

        for(String s: binaryTreePaths(root.left)){ // good use of for loop
            list.add(root.val + "->" + s); // recursive: assume the sub-problem is already solved
        }

        for(String s: binaryTreePaths(root.right)){
            list.add(root.val + "->" + s);
        }

        return list;
    }

    public List<String> binaryTreePaths(TreeNode root) { //dfs
        List<String> list = new LinkedList<>();
        Deque<TreeNode> nodeStack = new LinkedList<>();
        Deque<String> stringStack = new LinkedList<>();
        
        if(root == null){
            return list;
        }
        nodeStack.push(root);
        stringStack.push("");
        TreeNode curNode;
        String curStr;
        
        while(!nodeStack.isEmpty()){
            curNode = nodeStack.pop();
            curStr = stringStack.pop();
            
            if(curNode.left==null && curNode.right == null){
                list.add(curStr+curNode.val); //everything is done, add back to the list
            }
            if(curNode.left!=null){
                nodeStack.push(curNode.left);
                stringStack.push(curStr+curNode.val+"->");//not done yet, push to stack, not adding to list
            }
            if(curNode.right!=null){
                nodeStack.push(curNode.right);
                stringStack.push(curStr+curNode.val+"->");//not done yet, push to stack, not adding to list
            }
        }
        return list;
    }

    public List<String> binaryTreePaths(TreeNode root) {//bfs
        List<String> list = new LinkedList<>();
        Queue<TreeNode> nodeQueue = new LinkedList<>();
        Queue<String> strQueue = new LinkedList<>();
        
        if(root == null) return list;
        nodeQueue.add(root);
        strQueue.add("");
        TreeNode curNode;
        String curStr;
        
        while(!nodeQueue.isEmpty()){
            curNode = nodeQueue.poll();
            curStr = strQueue.poll();
            
            if(curNode.left==null && curNode.right==null){
                list.add(curStr + curNode.val);
            }
            if(curNode.left!=null){
                nodeQueue.add(curNode.left);
                strQueue.add(curStr + curNode.val + "->");
            }
            if(curNode.right!=null){
                nodeQueue.add(curNode.right);
                strQueue.add(curStr + curNode.val + "->");
            }
        }
        return list;
    }

}