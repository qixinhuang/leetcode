// Given a binary search tree (BST) with duplicates, find all the mode(s) (the most frequently occurred element) in the given BST.

// Assume a BST is defined as follows:

// The left subtree of a node contains only nodes with keys less than or equal to the node's key.
// The right subtree of a node contains only nodes with keys greater than or equal to the node's key.
// Both the left and right subtrees must also be binary search trees.
// For example:
// Given BST [1,null,2,2],
//    1
//     \
//      2
//     /
//    2
// return [2].

// Note: If a tree has more than one mode, you can return them in any order.

// Follow up: Could you do that without using any extra space? (Assume that the implicit stack space incurred due to recursion does not count).

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */


// This question is hard for me. When I first try to directly use subproblem in the recursion, it's very cumbersome to do.
// The key idea int the the following solution is in-order traversal and we keep a variable prev, it stores the node value of the latest traversed node. It actually stores the largest value so far during the traversal because of the property of BST and the fact that we are doing an in-order traversal. Since we are doing an in-order traversal, when evaluating root node, prev stores the value of the latest traversed(the most far down right) node in the left child tree, which is also the largest value in the left child tree. When evaluating right child tree, prev stores the root node value.

public class LC501 {
    int max = 0;
    int count = 0;
    Integer prev = null;
    public int[] findMode(TreeNode root) {
        List<Integer> list = new LinkedList<Integer>();
        
        recursiveHelper(root,list);
        
        int[] res = new int[list.size()];
        int i = 0;
        for(Integer num: list){
            res[i++] = num;
        }
        return res;
    }
    
    private void recursiveHelper(TreeNode root, List<Integer> list){
        if(root == null){
            return;
        }
        recursiveHelper(root.left, list);
        if(prev != null){
            if(prev == root.val){
                count ++;
            }else{
                count = 1;
            }
        }else{
            prev = root.val;
            count ++;
        }
        if(count > max){
            max = count;
            list.clear();
            list.add(root.val);
        }else if(count == max){
            list.add(root.val);
        }
        prev = root.val;
        recursiveHelper(root.right, list);
    }
}