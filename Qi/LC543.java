// 543. Diameter of Binary Tree
// Given a binary tree, you need to compute the length of the diameter of the tree. The diameter of a binary tree is the length of the longest path between any two nodes in a tree. This path may or may not pass through the root.

// Example:
// Given a binary tree 
//           1
//          / \
//         2   3
//        / \     
//       4   5    
// Return 3, which is the length of the path [4,2,1,3] or [5,2,1,3].

// Note: The length of path between two nodes is represented by the number of edges between them.

class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
}

public class LC543 {
	// Solution 1: Try to use sub problems when solve the problem. So The longest diameter could be from the three cases. First is from left tree, second is from right tree, and third is the longest path from left tree plus the longest path from right tree plus 2(if left and right tree are not null). 
    public int diameterOfBinaryTree(TreeNode root) {
        if(root == null){
            return 0;
        }
        int leftLength = diameterOfBinaryTree(root.left);
        int rightLength = diameterOfBinaryTree(root.right);
        int midLength = 0;
        if( (longestPath(root.left) == 0) && (longestPath(root.right) == 0) ){
            midLength = 0;
        }
        if( longestPath(root.left) == 0 ){
            midLength = longestPath(root.right) - 1 + 1;
        }
        if( longestPath(root.right) == 0 ){
            midLength = longestPath(root.left) - 1 + 1;
        }
        midLength = longestPath(root.left) - 1 + longestPath(root.right) - 1 + 2;
        return Math.max(midLength, Math.max(leftLength, rightLength));
    }
    
    public int longestPath(TreeNode root){
        if(root == null){
            return 0;
        }
        int left = longestPath(root.left) + 1;
        int right = longestPath(root.right) + 1;
        return Math.max(left, right);
    }


    // Solution 2: Even though the longest path might not go through the original root, however, the longest path has go through some node in the tree as if that node is the root. Add a global max value, every time we do left + right, we update the max value.
    int max = 0;
    
    public int diameterOfBinaryTree2(TreeNode root) {
        maxDepth(root);
        return max;
    }
    
    private int maxDepth(TreeNode root) {
        if (root == null) return 0;
        
        int left = maxDepth(root.left);
        int right = maxDepth(root.right);
        
        max = Math.max(max, left + right);
        
        return Math.max(left, right) + 1;
    }
}