# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def isSameTree2(self, p, q):
        """
            :type p: TreeNode
            :type q: TreeNode
            :rtype: bool
            """
        if p == None and q == None:
            return True
        elif p!= None and q != None:
            if p.val == q.val and self.isSameTree(p.left,q.left) and self.isSameTree(p.right,q.right):
                return True
            else:
                return False
        else:
            return False
    def isSameTree(self, p, q): ## non recursive
        pstack,qstack = [],[]
        if p != None:
            pstack.append(p)
        if q != None:
            qstack.append(q)
        while pstack and qstack:
            pnode = pstack.pop()
            qnode = qstack.pop()
            if pnode.val != qnode.val:
                return False
            else:
                if pnode.right != None:
                    pstack.append(pnode.right)
                if qnode.right != None:
                    qstack.append(qnode.right)
                if len(pstack) != len(qstack):
                    return False
                if pnode.left != None:
                    pstack.append(pnode.left)
                if qnode.left != None:
                    qstack.append(qnode.left)
                if len(pstack) != len(qstack):
                    return False
        return len(pstack) == len(qstack)
