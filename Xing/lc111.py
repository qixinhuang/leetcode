# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None


class Solution(object):
    def minDepth(self, root):
        """
            :type root: TreeNode
            :rtype: int
            """
        if root == None:
            return 0
        elif root != None and root.left == None and root.right == None:
            return 1
        else:
            left_height = self.minDepth(root.left)
            
            right_height = self.minDepth(root.right)
            
            if left_height == 0 or right_height == 0:
                return left_height + right_height + 1
            else:
                return 1 + min(right_height,left_height)

