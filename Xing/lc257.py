# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def binaryTreePaths(self, root):
        """
            :type root: TreeNode
            :rtype: List[str]
            """
        if root == None:
            return []
        elif root.left == None and root.right == None:
            return [str(root.val)]
        else:
            if root.left != None:
                left = [str(root.val) + '->' + x for x in self.binaryTreePaths(root.left)]
            else:
                left = []
            if root.right != None:
                right = [str(root.val) + '->' + x for x in self.binaryTreePaths(root.right)]
            else:
                right = []
            return left + right
