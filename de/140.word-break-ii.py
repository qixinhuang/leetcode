class Solution(object):
    def wordBreak(self, s, wordDict):
        """
        :type s: str
        :type wordDict: List[str]
        :rtype: List[str]
        """
        def helper(s, start, solutions, dict_set):
            if solutions[start] is not None:
                return

            solutions[start]=[]
            if s[start:] in dict_set:
                solutions[start].append(s[start:])
                
            for i in range(start+1, len(s)):
                prefix = s[start:i]
                if prefix in dict_set:
                    helper(s, i, solutions, dict_set)
                    for res in solutions[i]:
                        solutions[start].append(prefix+" "+res)
        
        solutions = [None]*len(s)
        helper(s, 0, solutions, set(wordDict))
        return solutions[0]
