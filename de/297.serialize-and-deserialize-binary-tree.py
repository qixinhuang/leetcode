# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
from collections import deque

class Codec:

    def serialize(self, root):
        """Encodes a tree to a single string.
        
        :type root: TreeNode
        :rtype: str
        """
        res = []
        q = deque([root])
        while q:
            cur = q.popleft()
            if cur is None:
                res.append('#')
            else:
                res.append(str(cur.val))
                q.append(cur.left)
                q.append(cur.right)
        return ','.join(res)
        

    def deserialize(self, data):
        """Decodes your encoded data to tree.
        
        :type data: str
        :rtype: TreeNode
        """
        def getnext(data, idx):
            start = idx
            while idx<len(data) and data[idx]!=',':
                idx+=1
            ret = data[start:idx]
            return ret, idx+1
        
        c, idx = getnext(data,0)
        root = None if c=='#' else TreeNode(int(c))
        q = deque([root])
        while q:
            cur = q.popleft()
            if cur:
                cleft, idx = getnext(data, idx)
                cright, idx = getnext(data, idx)
                cur.left = None if cleft=='#' else TreeNode(int(cleft))
                cur.right = None if cright=='#' else TreeNode(int(cright))
                q.append(cur.left)
                q.append(cur.right)
        return root

# Your Codec object will be instantiated and called as such:
# codec = Codec()
# codec.deserialize(codec.serialize(root))
