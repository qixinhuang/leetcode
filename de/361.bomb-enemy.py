class Solution(object):
    def maxKilledEnemies(self, grid):
        """
        :type grid: List[List[str]]
        :rtype: int
        """
        m=len(grid)
        if m==0: return 0
        n=len(grid[0])
        rowcount, colcounts= 0, [0]*n
        maxCount = 0
        for i in range(m):
            for j in range(n):
                if grid[i][j]!='W' and (j==0 or grid[i][j-1]=='W'):
                    rowcount, k = 0, j
                    while k<n and grid[i][k]!='W':
                        rowcount+=(grid[i][k]=='E')
                        k+=1
                    
                if grid[i][j]!='W' and (i==0 or grid[i-1][j]=='W'):
                    colcounts[j], k = 0, i
                    while k<m and grid[k][j]!='W':
                        colcounts[j]+=(grid[k][j]=='E')
                        k+=1

                if grid[i][j]=='0':
                    maxCount = max(maxCount, rowcount+colcounts[j])
            
        return maxCount
