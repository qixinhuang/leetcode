class Solution(object):
    def updateMatrix(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[List[int]]
        """
        m,n=len(matrix),len(matrix[0])
        q = [(i,j) for i in range(m) for j in range(n) if matrix[i][j]==0]
        visited = {p for p in q}
        step = 1
        dirs = [(-1,0),(1,0),(0,-1),(0,1)]
        res = [[0]*n for _ in range(m)]
        while q:
            next = []
            for x,y in q:
                for d in dirs:
                    x0,y0=x+d[0],y+d[1]
                    if 0<=x0<m and 0<=y0<n and (x0,y0) not in visited:
                        visited.add((x0,y0))
                        res[x0][y0]=step
                        next.append((x0,y0))
            step+=1
            q = next
        return res
